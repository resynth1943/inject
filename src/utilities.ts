export function hasOwnProperty (object: any, key: any) {
    return Object.prototype.hasOwnProperty.call(object, key);
}
