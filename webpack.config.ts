import { Configuration } from 'webpack';
import path from 'path';

const DEV = process.env.NODE_ENV === 'development';

const configuration: Configuration = {
    mode: DEV ? 'none' : 'production',
    entry: './src/index.ts',
    output: {
        path: path.resolve(__dirname, 'lib'),
        filename: DEV ? 'inject.js' : 'inject.min.js',
        libraryExport: 'umd',
        library: 'inject'
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    }
}

export default configuration;
