"use strict";
exports.__esModule = true;
var inject_1 = require("@resynth1943/inject");
var logger_1 = require("./logger");
exports.$Console = inject_1.createKey('console');
exports.$Message = inject_1.createKey('message');
var appDomain = {
    providers: [
        inject_1.provide(exports.$Console, console),
        inject_1.provide(exports.$Message, 'Hello, world!')
    ]
};
inject_1.runInDomain(appDomain, function () {
    logger_1.LoggerService.log('hello, world!');
    var message = inject_1.useKey(exports.$Message);
    if (typeof document === 'object') {
        document.body.innerHTML = message;
    }
    else {
        logger_1.LoggerService.log(message);
    }
});
logger_1.LoggerService.log('this will fail');
