# `@resynth1943/inject` example

This package is a brief example of the functionality of Inject. To see more about Inject and how it works, [visit the Codeberg repository](https://codeberg.org/resynth1943/inject).

## License

This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.
