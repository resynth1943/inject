import test from 'ava';
import { provide, isProvider } from '../src/providers';
import { $Key, specProvider } from './spec-data';

const provider = provide($Key, 2);

test('a provider\'s shape is known', t => {
    t.deepEqual(provider, { key: $Key, provide: 2 });
});

test('isProvider functions correctly', t => {
    t.true(isProvider(specProvider));
    t.false(isProvider({}));
});
