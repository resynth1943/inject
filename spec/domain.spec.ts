import test from 'ava';
import { useKey } from '../src/key';
import { runInDomain, assertNotInDomain, assertInDomain, Domain, wrapInCurrentDomain } from '../src/domain';
import { $Key, specDomain, keyValue, overrideDomain, overrideKeyValue, $Null, domainWithServiceProvider, OverridenSpecService, specDomainWithService, SpecService } from './spec-data';
import { useService } from '../src/service';

test('Requesting a key will fail outside of a domain', t => {
    t.throws(() => {
        useKey($Key);
    });
});

test('Requesting a key inside a domain will succeed', t => {
    runInDomain(specDomain, () => {
        const value = useKey($Key);
        t.deepEqual(value, keyValue);
    });
});

test('Running a domain inside a domain will override providers of the parent', t => {
    runInDomain(specDomain, () => {
        runInDomain(overrideDomain, () => {
            const value = useKey($Key);
            t.deepEqual(
                value,
                overrideKeyValue
            );
        });
    });
});

test('Nesting domains will still disallow unresolvable keys', t => {
    runInDomain(specDomain, () => {
        runInDomain(overrideDomain, () => {
            t.throws(() => useKey($Null));
        });
    })
});

test('assertNotInDomain works', t => {
    runInDomain(specDomain, () => {
        t.throws(() => assertNotInDomain());
    });

    assertNotInDomain();
});

test('assertInDomain works', t => {
    runInDomain(specDomain, () => {
        assertInDomain();
    });

    t.throws(() => assertInDomain());
});

test('placing providers in `services` works', t => {
    runInDomain(domainWithServiceProvider, () => {
        const service = useService(OverridenSpecService);
        t.deepEqual(service.keyValue, keyValue);
    });
});

test('placing services in `services` works', t => {
    runInDomain(specDomainWithService, () => {
        const service = useService(SpecService);
        t.deepEqual(service.keyValue, keyValue);
    });
});

test('wrapInCurrentDomain functions correctly', t => {
	const passedArgs = [1, 2, 3];
	const thisContext = {};
	let wrapped: Function;

	runInDomain(specDomain, () => {
		wrapped = wrapInCurrentDomain(function (this: unknown, ...args: unknown[]) {
			t.notThrows(() => {
				t.deepEqual(useKey($Key), keyValue);
			});

			t.deepEqual(args, passedArgs);
			t.deepEqual(this, thisContext);
		});
	});

	// @ts-ignore
	wrapped.call(thisContext, ...passedArgs);
});
