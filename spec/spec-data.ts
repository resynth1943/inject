import { createKey } from '../src/key';
import { Domain } from '../src/domain';
import { provide } from '../src/providers';
import { createService } from '../src/service';

export const $Key = createKey<number>('spec key');
export const keyValue = 2;
export const overrideKeyValue = 4;

export const $Null = createKey<null>('null');

export const specDomain: Domain = {
    providers: [provide($Key, 2)]
};

export const SpecService = createService({
    keyValue
});

export const NullSpecService = createService({
    null: null
});

export const OverridenSpecService = createService({
    ...SpecService
});

export const ExplicitlyDefinedService = createService({
    ...SpecService
});

export const specProvider = provide($Key, 2);

export const specDomainWithService: Domain = {
    ...specDomain,
    services: [SpecService]
};

export const $SpecService = createKey<typeof SpecService>('specservice');

export const domainWithServiceProvider: Domain = {
    ...specDomain,
    services: [provide($SpecService, OverridenSpecService)]
}

export const overrideDomain: Domain = {
    providers: [provide($Key, 4)]
}
