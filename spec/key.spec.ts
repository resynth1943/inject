import test from 'ava';
import { runInDomain } from '../src/domain';
import { specDomain, $Null } from './spec-data';
import { useKey } from '../src/key';

test('useKey calls with a default will not error with unresolvable key', t => {
    runInDomain(specDomain, () => {
        const value = useKey($Null, null);
        t.deepEqual(value, null);
    });
});

test('useKey calls without a default and an unresolvable key will throw', t => {
    runInDomain(specDomain, () => {
        t.throws(() => useKey($Null));
    });
});

test('calling useKey outside of a domain will throw', t => {
    t.throws(() => useKey($Null));
});
