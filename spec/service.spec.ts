import test from 'ava';
import { createService, assertServiceIsStamped, assertServiceIsNotStamped, provideService, useService } from '../src/service';
import { runInDomain } from '../src/domain';
import { specDomain, SpecService, keyValue, NullSpecService, ExplicitlyDefinedService } from './spec-data';

test('createService returns an equal object', t => {
    const service = createService({});
    t.deepEqual(service, {});
});

test('createService can be called inside a domain', t => {
    runInDomain(specDomain, () => {
        const service = createService({});
        t.deepEqual(service, {});
    });
});

test('assertServiceIsStamped works correctly', t => {
    t.throws(() => assertServiceIsStamped({}));
    const service = createService({});
    assertServiceIsStamped(service);
});

test('assertServiceIsNotStamped works', t => {
    assertServiceIsNotStamped({});
    const service = createService({});
    t.throws(() => assertServiceIsNotStamped(service));
});

test('provideService returns a provider with a unique key', t => {
    const service = createService({});
    const override = createService({});
    const provider = provideService(service, override);

    t.deepEqual(provider.provide, override);
});

test('useService functions correctly', t => {
    runInDomain(specDomain, () => {
        const service = useService(SpecService);
        t.deepEqual(service.keyValue, keyValue);
    });
});
