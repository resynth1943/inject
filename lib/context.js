"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var key_1 = require("./key");
var service_1 = require("./service");
function createContext(descriptor) {
    var context = {};
    var _loop_1 = function (key) {
        var value = descriptor[key];
        var isKey = typeof value === 'symbol';
        Object.defineProperty(context, key, {
            get: function () {
                return isKey ? key_1.useKey(value) : service_1.useService(value);
            }
        });
    };
    for (var _i = 0, _a = Object.keys(descriptor); _i < _a.length; _i++) {
        var key = _a[_i];
        _loop_1(key);
    }
    return context;
}
exports.createContext = createContext;
