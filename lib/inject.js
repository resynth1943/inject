var inject =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var domain_1 = __webpack_require__(1);
exports.runInDomain = domain_1.runInDomain;
var key_1 = __webpack_require__(5);
exports.createKey = key_1.createKey;
exports.Nothing = key_1.Nothing;
exports.useKey = key_1.useKey;
var providers_1 = __webpack_require__(2);
exports.provide = providers_1.provide;
exports.isProvider = providers_1.isProvider;
var service_1 = __webpack_require__(4);
exports.createService = service_1.createService;
exports.provideService = service_1.provideService;
exports.useService = service_1.useService;


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var providers_1 = __webpack_require__(2);
var service_1 = __webpack_require__(4);
/** Asserts this execution context is not inside a domain. */
function assertNotInDomain(message) {
    if (exports.currentDomain) {
        throw new Error(message || "This code is already running inside a domain execution context.");
    }
}
exports.assertNotInDomain = assertNotInDomain;
/** Asserts this execution contect is inside a domain. */
function assertInDomain(message) {
    if (!exports.currentDomain) {
        throw new Error(message || 'This code needs to run inside a domain execution context.');
    }
}
exports.assertInDomain = assertInDomain;
/** Creates an internal representation of a `Domain`. */
function createInternalDomain(domain) {
    var _a, _b;
    return __assign(__assign({}, domain), { $$: new Map(__spreadArrays(domain.providers.map(function (provider) { return [provider.key, provider.provide]; }), ((_b = (_a = domain.services) === null || _a === void 0 ? void 0 : _a.map(function (serviceDescriptor) {
            if (providers_1.isProvider(serviceDescriptor)) {
                service_1.assertServiceIsStamped(serviceDescriptor.provide);
                return [serviceDescriptor.key, serviceDescriptor.provide];
            }
            else {
                service_1.assertServiceIsStamped(serviceDescriptor);
                var key = service_1.serviceKeyMap.get(serviceDescriptor);
                return [key, serviceDescriptor];
            }
        })) !== null && _b !== void 0 ? _b : []))) });
}
/** Runs a function inside the execution context of a domain. */
function runInDomain(baseDomain, callback) {
    var _a, _b, _c;
    var oldDomain = exports.currentDomain;
    var domain = exports.currentDomain ? createInternalDomain(__assign(__assign(__assign({}, exports.currentDomain), baseDomain), { providers: __spreadArrays(baseDomain.providers, exports.currentDomain.providers.filter(function (provider) { return !baseDomain.providers.find(function (xProvider) { return xProvider.key === provider.key; }); })), services: __spreadArrays(((_a = baseDomain.services) !== null && _a !== void 0 ? _a : []), ((_c = (_b = exports.currentDomain.services) === null || _b === void 0 ? void 0 : _b.filter(function (service) { var _a; return !((_a = exports.currentDomain.services) === null || _a === void 0 ? void 0 : _a.includes(service)); })) !== null && _c !== void 0 ? _c : [])) })) : createInternalDomain(baseDomain);
    exports.currentDomain = domain;
    callback(domain);
    exports.currentDomain = oldDomain;
}
exports.runInDomain = runInDomain;


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var utilities_1 = __webpack_require__(3);
function provide(key, provide) {
    return { key: key, provide: provide };
}
exports.provide = provide;
function isProvider(thing) {
    return typeof thing === 'object' && typeof thing.key === 'symbol' && utilities_1.hasOwnProperty(thing, 'provide');
}
exports.isProvider = isProvider;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function hasOwnProperty(object, key) {
    return Object.prototype.hasOwnProperty.call(object, key);
}
exports.hasOwnProperty = hasOwnProperty;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var key_1 = __webpack_require__(5);
var domain_1 = __webpack_require__(1);
exports.serviceKeyMap = new WeakMap();
function createService(service) {
    assertServiceIsNotStamped(service);
    exports.serviceKeyMap.set(service, key_1.createKey('GenericServiceKey'));
    // For now...
    return service;
}
exports.createService = createService;
function assertServiceIsStamped(service, message) {
    if (!exports.serviceKeyMap.has(service)) {
        throw new Error(message || 'Did you forget to wrap this service in `createService`?');
    }
}
exports.assertServiceIsStamped = assertServiceIsStamped;
function assertServiceIsNotStamped(service, message) {
    if (exports.serviceKeyMap.has(service)) {
        throw new Error(message || "This service has already been registered!");
    }
}
exports.assertServiceIsNotStamped = assertServiceIsNotStamped;
function provideService(service, override) {
    assertServiceIsStamped(service);
    var key = exports.serviceKeyMap.get(service);
    return {
        key: key,
        provide: override
    };
}
exports.provideService = provideService;
function useService(service) {
    var _a;
    domain_1.assertInDomain();
    assertServiceIsStamped(service);
    var domain = domain_1.currentDomain;
    var key = exports.serviceKeyMap.get(service);
    var override = (_a = domain.$$.get(key)) === null || _a === void 0 ? void 0 : _a.provide;
    return override !== null && override !== void 0 ? override : service;
}
exports.useService = useService;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var domain_1 = __webpack_require__(1);
exports.Nothing = Object.assign(Symbol('DI.Nothing'), { __nothing__: true });
function createKey(name) {
    return Symbol("DI.Key(" + name + ")");
}
exports.createKey = createKey;
function useKey(key, defaultValue) {
    if (defaultValue === void 0) { defaultValue = exports.Nothing; }
    domain_1.assertInDomain();
    var internalCurrentDomain = domain_1.currentDomain;
    var $$ = internalCurrentDomain.$$;
    var value = $$.has(key) ? $$.get(key) : defaultValue;
    if (value === exports.Nothing) {
        throw new Error("The current domain does not have a provider for key \"" + String(key) + "\", and a default value has not been provided.");
    }
    return value;
}
exports.useKey = useKey;


/***/ })
/******/ ])["umd"];