"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function hasOwnProperty(object, key) {
    return Object.prototype.hasOwnProperty.call(object, key);
}
exports.hasOwnProperty = hasOwnProperty;
