"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var providers_1 = require("./providers");
var service_1 = require("./service");
/** Asserts this execution context is not inside a domain. */
function assertNotInDomain(message) {
    if (exports.currentDomain) {
        throw new Error(message || "This code is already running inside a domain execution context.");
    }
}
exports.assertNotInDomain = assertNotInDomain;
/** Asserts this execution contect is inside a domain. */
function assertInDomain(message) {
    if (!exports.currentDomain) {
        throw new Error(message || 'This code needs to run inside a domain execution context.');
    }
}
exports.assertInDomain = assertInDomain;
/** Creates an internal representation of a `Domain`. */
function createInternalDomain(domain) {
    var _a, _b;
    return __assign(__assign({}, domain), { $$: new Map(__spreadArrays(domain.providers.map(function (provider) { return [provider.key, provider.provide]; }), ((_b = (_a = domain.services) === null || _a === void 0 ? void 0 : _a.map(function (serviceDescriptor) {
            if (providers_1.isProvider(serviceDescriptor)) {
                service_1.assertServiceIsStamped(serviceDescriptor.provide);
                return [serviceDescriptor.key, serviceDescriptor.provide];
            }
            else {
                service_1.assertServiceIsStamped(serviceDescriptor);
                var key = service_1.serviceKeyMap.get(serviceDescriptor);
                return [key, serviceDescriptor];
            }
        })) !== null && _b !== void 0 ? _b : []))) });
}
/** Runs a function inside the execution context of a domain. */
function runInDomain(baseDomain, callback) {
    var _a, _b, _c;
    var oldDomain = exports.currentDomain;
    var domain = exports.currentDomain ? createInternalDomain(__assign(__assign(__assign({}, exports.currentDomain), baseDomain), { providers: __spreadArrays(baseDomain.providers, exports.currentDomain.providers.filter(function (provider) { return !baseDomain.providers.find(function (xProvider) { return xProvider.key === provider.key; }); })), services: __spreadArrays(((_a = baseDomain.services) !== null && _a !== void 0 ? _a : []), ((_c = (_b = exports.currentDomain.services) === null || _b === void 0 ? void 0 : _b.filter(function (service) { var _a; return !((_a = exports.currentDomain.services) === null || _a === void 0 ? void 0 : _a.includes(service)); })) !== null && _c !== void 0 ? _c : [])) })) : createInternalDomain(baseDomain);
    exports.currentDomain = domain;
    callback(domain);
    exports.currentDomain = oldDomain;
}
exports.runInDomain = runInDomain;
/**
 * Creates a wrapped version of the passed callback that, when called, is invoked in the context of the current domain execution context.
 * This is especially useful for `queueMicrotask`, which, when called from inside a domain, doesn't run in the current domain.
 * Instead, it runs *outside* of the current domain, which is very confusing when working with asynchronous data streams.
*/
function wrapInCurrentDomain(callback) {
    assertInDomain("wrapInCurrentDomain can only be called from inside a domain execution context.");
    // Capture the current domain so we can wrap the callback in it.
    var domain = exports.currentDomain;
    return function wrappedCallback() {
        var _this = this;
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        assertNotInDomain("The returned callback of wrapInCurrentDomain must not be called from inside a domain execution context.");
        // Capture the return value, so we can return it later on.
        var returnValue = undefined;
        runInDomain(domain, function () {
            returnValue = callback.call.apply(callback, __spreadArrays([_this], args));
        });
        return returnValue;
    };
}
exports.wrapInCurrentDomain = wrapInCurrentDomain;
