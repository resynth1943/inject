"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var domain_1 = require("./domain");
exports.Nothing = Object.assign(Symbol('DI.Nothing'), { __nothing__: true });
function createKey(name) {
    return Symbol("DI.Key(" + name + ")");
}
exports.createKey = createKey;
function useKey(key, defaultValue) {
    if (defaultValue === void 0) { defaultValue = exports.Nothing; }
    domain_1.assertInDomain();
    var internalCurrentDomain = domain_1.currentDomain;
    var $$ = internalCurrentDomain.$$;
    var value = $$.has(key) ? $$.get(key) : defaultValue;
    if (value === exports.Nothing) {
        throw new Error("The current domain does not have a provider for key \"" + String(key) + "\", and a default value has not been provided.");
    }
    return value;
}
exports.useKey = useKey;
