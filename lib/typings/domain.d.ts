import { Key } from './key';
import { Provider } from './providers';
import { Service } from './service';
/** Describes a domain with providers. */
export interface Domain {
    readonly providers: readonly Provider<Key<unknown>>[];
    readonly services?: readonly (Service | Provider<Key<Service>>)[];
}
/** Describes an internal representation of a domain. This uses a cache to speed up lookup times. */
export interface InternalDomain extends Domain {
    readonly $$: Map<Key<unknown>, Provider<Key<unknown>>>;
}
/** The current domain set for this execution context. */
export declare let currentDomain: InternalDomain | void;
/** Asserts this execution context is not inside a domain. */
export declare function assertNotInDomain(message?: string): void;
/** Asserts this execution contect is inside a domain. */
export declare function assertInDomain(message?: string): void;
/** Runs a function inside the execution context of a domain. */
export declare function runInDomain(baseDomain: Domain, callback: (domain: Domain) => void): void;
/**
 * Creates a wrapped version of the passed callback that, when called, is invoked in the context of the current domain execution context.
 * This is especially useful for `queueMicrotask`, which, when called from inside a domain, doesn't run in the current domain.
 * Instead, it runs *outside* of the current domain, which is very confusing when working with asynchronous data streams.
*/
export declare function wrapInCurrentDomain<TThis, TArg, TReturn, TFunction extends (this: TThis, ...args: TArg[]) => TReturn>(callback: TFunction): TFunction;
