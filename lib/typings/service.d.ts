import { Key } from './key';
import { Provider } from './providers';
export declare type Service = Record<PropertyKey, unknown>;
export declare type StampedService = Service & {
    __is_a_service__: true;
};
export declare const serviceKeyMap: WeakMap<StampedService, Key<Record<string | number | symbol, unknown>>>;
export declare function createService<TService extends Service>(service: TService): TService;
export declare function assertServiceIsStamped(service: Service, message?: string): void;
export declare function assertServiceIsNotStamped(service: Service, message?: string): void;
export declare function provideService<TService extends Service, TOverridingService extends TService>(service: TService, override: TOverridingService): Provider;
export declare function useService<TService extends Service>(service: TService): TService;
